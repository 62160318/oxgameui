/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthorn.oxgameui;

import java.io.Serializable;

/**
 *
 * @author ACER
 */
public class Player implements Serializable {
    private char name;
    private int win;
    private int lose;
    private int draw;

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }
    public void win(){
        win++;
    }
    
    public int getLose() {
        return lose;
    }
    public void lose(){
        lose++;
    }

    public int getDraw() {
        return draw;
    }
    public void draw(){
        draw++;
    }
    

  public Player(char name){
        this.name = name;
    }

    void Lose() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
}
