/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthorn.oxgameui;

import java.io.Serializable;

/**
 *
 * @author ACER
 */
public class Table implements Serializable {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'},};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastrow;
    private int lastcol;
    private Boolean Finish = false;
    private Player winner;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println();
        }
    }

    public boolean setRowCol(int row, int col) {
        if(isFinish())return false;
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            checkWin();
            return true;
        }
        return false;
    }
     public char getRowCol(int row ,int col){
        return table[row][col];
    }

    public Player getcurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    public void checkcol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public void checkrow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }
   

    public void checkX() {
        for (int row = 0; row < table.length; row++) {
                if (table[0][0] == currentPlayer.getName()
                        && table[1][1] == currentPlayer.getName()
                        && table[2][2] == currentPlayer.getName()) {
                    Finish = true;
                    winner = currentPlayer;
                    setStatWinLose();
                    return;
                } else if (table[0][2] == currentPlayer.getName()
                        && table[1][1] == currentPlayer.getName()
                        && table[2][0] == currentPlayer.getName()) {
                    Finish = true;
                    winner = currentPlayer;
                    setStatWinLose();
                    return;
                }
         
        }

    }
    public void checkDraw(){
        playerO.draw();
        playerX.draw();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }


        public void checkWin() {
        checkcol();
        checkrow();
        checkX();
    }
     public boolean isFinish(){
        return Finish;
    }
     public Player getWinner(){
         return winner;
     }
}
